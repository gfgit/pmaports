# Maintainer: Svyatoslav Ryhel <clamor95@gmail.com>

pkgname=linux-postmarketos-grate
pkgver=6.6.7
pkgrel=0
pkgdesc="Linux kernel with experimental patches for Tegra"
arch="armv7"
url="https://gitlab.com/grate-driver/linux"
license="GPL-2.0-only"
options="!strip !check !tracedeps pmb:cross-native pmb:kconfigcheck-community"
makedepends="bash bison findutils flex postmarketos-installkernel openssl-dev
	     perl gmp-dev mpc1-dev mpfr-dev xz"

# Source
_flavor="${pkgname#linux-}"
_tag="v${pkgver//_/-}-stable"
_carch="arm"
_config="config-$_flavor.$arch"
source="$pkgname-$_tag.tar.bz2::$url/-/archive/$_tag/linux-$_tag.tar.bz2
	$_config"
builddir="$srcdir/linux-$_tag"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$CARCH" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-postmarketOS"
}

package() {
	mkdir -p "$pkgdir"/boot
	make zinstall modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_DTBS_PATH="$pkgdir"/boot/dtbs
	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir"/usr/share/kernel/$_flavor/kernel.release
}

sha512sums="
734bf6b2132a45eb0768f5793a0d39815390500af84608080493811603bcf2b8e95b13a4478c7b1a0e91bd16e632cb4e68588494af6406901ca5de5eb7700ebc  linux-postmarketos-grate-v6.6.7-stable.tar.bz2
442c97e4d59d6a2c975e01f9e56886c8c22dc5a2c9b9b1f07d7cf21599392ea221f48e146f485d4bcd9ff5ef23f3ea634862d339122e9e6cd85c77a68c1d9f74  config-postmarketos-grate.armv7
"
